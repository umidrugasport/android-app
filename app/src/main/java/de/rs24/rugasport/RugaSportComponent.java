package de.rs24.rugasport;

import javax.inject.Singleton;

import dagger.Component;
import de.rs24.rugasport.activity.MainActivity;
import de.rs24.rugasport.activity.ProductTableActivity;
import de.rs24.rugasport.activity.adapter.MenuListAdapter;
import de.rs24.rugasport.activity.adapter.ProductListAdapter;
import de.rs24.rugasport.activity.fragment.MenFragment;
import de.rs24.rugasport.activity.fragment.WhatsNewFragment;
import de.rs24.rugasport.activity.fragment.WomenFragment;

@Singleton
@Component(
        modules = {
                RugaSportModule.class
        }
)
public interface RugaSportComponent {
    void inject(RugaSportApplication app);
    void inject(MainActivity activity);
    void inject(ProductTableActivity activity);
    void inject(WomenFragment fragment);
    void inject(MenFragment fragment);
    void inject(WhatsNewFragment fragment);
    void inject(MenuListAdapter.ViewHolder liastAdapter);
    void inject(ProductListAdapter.ViewHolder liastAdapter);
}