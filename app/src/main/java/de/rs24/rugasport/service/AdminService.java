package de.rs24.rugasport.service;

import javax.inject.Inject;

import de.rs24.rugasport.BuildConfig;
import de.rs24.rugasport.api.RugaSportCallback;
import de.rs24.rugasport.api.RugaSportCallbackConverter;
import de.rs24.rugasport.api.admin.AdminApi;
import de.rs24.rugasport.exceptions.ApiException;
import de.rs24.rugasport.model.admin.AdminModel;
import retrofit2.Call;

public class AdminService extends BaseService {

    @Inject
    AdminApi adminApi;

    @Inject
    public AdminService() {
    }

    public void getAdminToken(final RugaSportCallback<String> callback) {
        AdminModel adminModel = new AdminModel();
        adminModel.setUsername(BuildConfig.WS_USER);
        adminModel.setPassword(BuildConfig.WS_PASS);
            Call<String> call = adminApi.getAdminToken(adminModel);
            call.enqueue(new RugaSportCallbackConverter<String>(new RugaSportCallback<String>() {
                @Override
                public void onSuccess(String response) {
                    cacheManager.saveAdminToken(response);
                    callback.onSuccess(response);
                }

                @Override
                public void onFailure(ApiException ex) {
                    callback.onFailure(ex);
                }
            }, context, cacheManager));
    }
}
