package de.rs24.rugasport.service;

import javax.inject.Inject;

import de.rs24.rugasport.api.RugaSportCallback;
import de.rs24.rugasport.api.RugaSportCallbackConverter;
import de.rs24.rugasport.api.category.CategoryApi;
import de.rs24.rugasport.exceptions.ApiException;
import de.rs24.rugasport.model.category.CategoryModel;
import retrofit2.Call;

public class CategoryService extends BaseService {

    @Inject
    CategoryApi categoryApi;

    @Inject
    public CategoryService() {
    }

    public void getCategories(final RugaSportCallback<CategoryModel> callback) {
        Call<CategoryModel> call = categoryApi.get("Bearer "+cacheManager.getAdminToken());
        call.enqueue(new RugaSportCallbackConverter<CategoryModel>(new RugaSportCallback<CategoryModel>() {
            @Override
            public void onSuccess(CategoryModel response) {
                cacheManager.saveCategories(response);
                callback.onSuccess(response);
            }

            @Override
            public void onFailure(ApiException ex) {
                callback.onFailure(ex);
            }
        }, context, cacheManager));
    }
}