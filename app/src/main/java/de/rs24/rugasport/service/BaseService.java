package de.rs24.rugasport.service;

import android.content.Context;
import android.text.TextUtils;

import javax.inject.Inject;

import de.rs24.rugasport.cache.RugaSportCacheManager;

public class BaseService {

    @Inject
    public
    RugaSportCacheManager cacheManager;

    @Inject
    Context context;

}
