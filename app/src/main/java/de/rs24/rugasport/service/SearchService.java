package de.rs24.rugasport.service;

import javax.inject.Inject;

import de.rs24.rugasport.api.RugaSportCallback;
import de.rs24.rugasport.api.RugaSportCallbackConverter;
import de.rs24.rugasport.api.search.SearchApi;
import de.rs24.rugasport.exceptions.ApiException;
import de.rs24.rugasport.model.product.ItemsModel;
import retrofit2.Call;

import static de.rs24.rugasport.api.ApiConstants.ENTITY_ID;
import static de.rs24.rugasport.api.ApiConstants.QUICK_SEARCH_CONTAINER;
import static de.rs24.rugasport.api.ApiConstants.SEARCH_TERM;

public class SearchService extends BaseService {

    @Inject
    SearchApi searchApi;

    @Inject
    public SearchService() {
    }

    public void quickSearch(String value, final RugaSportCallback<ItemsModel> callback) {
        Call<ItemsModel> call = searchApi.quickSearch("Bearer "+cacheManager.getAdminToken(), QUICK_SEARCH_CONTAINER, SEARCH_TERM, value);
        call.enqueue(new RugaSportCallbackConverter<ItemsModel>(new RugaSportCallback<ItemsModel>() {
            @Override
            public void onSuccess(ItemsModel response) {
                callback.onSuccess(response);
            }

            @Override
            public void onFailure(ApiException ex) {
                callback.onFailure(ex);
            }
        }, context, cacheManager));
    }

    public void searchProductById(int id, final RugaSportCallback<ItemsModel> callback) {
        Call<ItemsModel> call = searchApi.searchProductById("Bearer "+cacheManager.getAdminToken(), id, ENTITY_ID);
        call.enqueue(new RugaSportCallbackConverter<ItemsModel>(new RugaSportCallback<ItemsModel>() {
            @Override
            public void onSuccess(ItemsModel response) {
                callback.onSuccess(response);
            }

            @Override
            public void onFailure(ApiException ex) {
                callback.onFailure(ex);
            }
        }, context, cacheManager));
    }
}