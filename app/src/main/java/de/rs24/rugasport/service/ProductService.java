package de.rs24.rugasport.service;

import javax.inject.Inject;

import de.rs24.rugasport.api.RugaSportCallback;
import de.rs24.rugasport.api.RugaSportCallbackConverter;
import de.rs24.rugasport.api.product.ProductApi;
import de.rs24.rugasport.exceptions.ApiException;
import de.rs24.rugasport.model.product.ItemsModel;
import retrofit2.Call;

import static de.rs24.rugasport.api.ApiConstants.CATEGORY_ID;

public class ProductService extends BaseService {

    @Inject
    ProductApi productApi;

    @Inject
    public ProductService() {
    }

    public void getTopProducts(int catId, int pageSize, int currentPage, final RugaSportCallback<ItemsModel> callback) {
        String query = "items[sku,name,price,custom_attributes]"; // TODO: as constants
        Call<ItemsModel> call = productApi.getTopProducts("Bearer "+cacheManager.getAdminToken(),query,CATEGORY_ID,catId,"eq",pageSize,currentPage);
        call.enqueue(new RugaSportCallbackConverter<ItemsModel>(new RugaSportCallback<ItemsModel>() {
            @Override
            public void onSuccess(ItemsModel response) {
                callback.onSuccess(response);
            }

            @Override
            public void onFailure(ApiException ex) {
                callback.onFailure(ex);
            }
        }, context, cacheManager));
    }
}