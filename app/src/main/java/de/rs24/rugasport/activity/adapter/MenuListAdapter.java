package de.rs24.rugasport.activity.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import de.rs24.rugasport.R;
import de.rs24.rugasport.RugaSportApplication;
import de.rs24.rugasport.activity.ProductTableActivity;
import de.rs24.rugasport.activity.fragment.WomenFragment;
import de.rs24.rugasport.cache.RugaSportCacheManager;
import de.rs24.rugasport.model.listModel.ListItemModel;

import static de.rs24.rugasport.activity.Constants.CATEGORY_ID;
import static de.rs24.rugasport.activity.Constants.CATEGORY_NAME;
import static de.rs24.rugasport.activity.Constants.FRAGMENT_ARG;
import static de.rs24.rugasport.activity.Constants.MEN_CAT;
import static de.rs24.rugasport.activity.Constants.PRODUCT_TABLE_ACTIVITY;
import static de.rs24.rugasport.activity.Constants.WOMEN_CAT;
import static de.rs24.rugasport.activity.Constants.WOMEN_SUB_CAT1;
import static de.rs24.rugasport.activity.Constants.WOMEN_SUB_CAT2;

public class MenuListAdapter extends RecyclerView.Adapter<MenuListAdapter.ViewHolder>{

    private List<ListItemModel> mItems;
    private Context mContext;
    private int itemLayout;

    private int itemIndex = 0;
    private String listTag = "";
    private List<Integer> itemLayouts = new ArrayList<Integer>();

    public MenuListAdapter(Context context, List<ListItemModel> items) {
        this.mContext = context;
        this.mItems = items;
    }

    public void setItemLayout(int itemLayout){
        this.itemLayout = itemLayout;
    }

    public void setListTag(String listTag){
        this.listTag = listTag;
    }

    @Override
    public void onViewAttachedToWindow(MenuListAdapter.ViewHolder holder) {
        super.onViewAttachedToWindow(holder);

        Activity activity = (Activity) mContext;
        ViewGroup viewGroup = (ViewGroup) ((ViewGroup) activity.findViewById(android.R.id.content)).getChildAt(0);

        RecyclerView recyclerView = (RecyclerView) viewGroup.findViewById(R.id.menu_list_recycler_view);
    }

    @Override
    public MenuListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        this.itemLayout = itemLayouts.get(itemIndex);
        itemIndex++;
        View itemView = inflater.inflate(itemLayout, parent, false);
        MenuListAdapter.ViewHolder viewHolder = new MenuListAdapter.ViewHolder(mContext, itemView, itemLayout);
        return viewHolder;
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBindViewHolder(MenuListAdapter.ViewHolder holder, int position) {

        final ListItemModel item = mItems.get(position);

        holder.itemKey.setText(item.getKey());
        holder.itemCatId = item.getCatId();
        holder.itemCatName = item.getCatName();
        if(holder.itemArrow!=null){
            if(item.getArrow()!=0) {
                holder.itemArrow.setImageResource(item.getArrow());
            }
        }

        if(holder.itemValue!=null){
            holder.itemValue.setText(item.getValue());
        }
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public void setItemLayouts( List<Integer> itemLayouts) {
        this.itemLayouts = itemLayouts;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @Inject
        RugaSportCacheManager cacheManager;

        public TextView itemKey;
        public TextView itemValue;
        public ImageView itemArrow;
        public View mView;
        public View itemLine;
        public RelativeLayout itemRelativeLayout;
        public Context mContext;
        public Activity activity;
        public ViewGroup viewGroup;
        public int itemCatId = 1;
        public String itemCatName = "";

        public ViewHolder(final Context mContext, View itemView, int itemLayout) {
            super(itemView);
            this.mContext = mContext;

            activity = (Activity) mContext;
            ((RugaSportApplication) activity.getApplication()).getRugaSportComponent().inject(this);
            viewGroup = (ViewGroup) ((ViewGroup) activity.findViewById(android.R.id.content)).getChildAt(0);

            itemRelativeLayout = (RelativeLayout) itemView.findViewById(R.id.item_view);
            itemLine = (View) itemView.findViewById(R.id.item_line);
            switch (itemLayout) {
                case R.layout.list_menu_item:
                case R.layout.list_menu_sub_item:
                    itemRelativeLayout.setOnClickListener(this);
                    itemKey = (TextView) itemView.findViewById(R.id.item_sku);
                    itemValue = (TextView) itemView.findViewById(R.id.item_name);
                    itemArrow = (ImageView) itemView.findViewById(R.id.item_image);
                    break;
                default:
                    break;
            }
            mView = itemView;
        }

        public ViewHolder(View itemView) {
            super(itemView);
        }

        @Override
        public void onClick(View v) {

            activity = (Activity) mContext;

            ((RugaSportApplication) activity.getApplication()).getRugaSportComponent().inject(this);

            viewGroup = (ViewGroup) ((ViewGroup) activity.findViewById(android.R.id.content)).getChildAt(0);

            Intent intent;
            String itemPosition = String.valueOf(getAdapterPosition());

            String nextListTag = "";
            switch (listTag) {
                case WOMEN_CAT:
                    nextListTag = WOMEN_SUB_CAT1;
                    if(getAdapterPosition() == 1){
                        nextListTag = WOMEN_SUB_CAT2;
                    }
                    attachFragment(new WomenFragment(),"",nextListTag);
                    break;
                case WOMEN_SUB_CAT1:
                case WOMEN_SUB_CAT2:
                    if((getAdapterPosition() == 0 && listTag.equals(WOMEN_SUB_CAT1))||(getAdapterPosition() == 1 && listTag.equals(WOMEN_SUB_CAT2))){
                        nextListTag = WOMEN_CAT;
                    }
                    else if(getAdapterPosition() == 0 && listTag.equals(WOMEN_SUB_CAT2)){
                        nextListTag = WOMEN_SUB_CAT1;
                    }
                    else if(getAdapterPosition() == getItemCount()-1 && listTag.equals(WOMEN_SUB_CAT1)){
                        nextListTag = WOMEN_SUB_CAT2;
                    }
                    if(nextListTag.equals("")){
                        cacheManager.saveLastActivity(PRODUCT_TABLE_ACTIVITY);
                        intent = new Intent(mContext, ProductTableActivity.class);
                        intent.putExtra(CATEGORY_ID, ""+itemCatId);
                        intent.putExtra(CATEGORY_NAME, itemCatName);
                        mContext.startActivity(intent);
                    }else {
                        attachFragment(new WomenFragment(), "", nextListTag);
                    }
                    break;
                case MEN_CAT:
                    nextListTag = WOMEN_SUB_CAT1;
                    if(getAdapterPosition() == 1){
                        nextListTag = WOMEN_SUB_CAT2;
                    }
                    attachFragment(new WomenFragment(),"",nextListTag);
                    break;
                default:
                    cacheManager.saveLastActivity(PRODUCT_TABLE_ACTIVITY);
                    intent = new Intent(mContext, ProductTableActivity.class);
                    intent.putExtra(CATEGORY_ID, ""+itemCatId);
                    intent.putExtra(CATEGORY_NAME, itemCatName);
                    mContext.startActivity(intent);
                    break;
            }
        }

        private void attachFragment(Fragment fragment, String tag, String extra) {
            FragmentTransaction ft = ((FragmentActivity) mContext).getSupportFragmentManager().beginTransaction();
            Bundle bundle = new Bundle();
            bundle.putString(FRAGMENT_ARG, extra);
            fragment.setArguments(bundle);
            ft.replace(R.id.content_main, fragment, tag);
            ft.commit();
        }
    }

    private void showSimpleDialog(String title, String message) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
        alertDialogBuilder.setTitle(title);
        alertDialogBuilder
                .setMessage(message)
                .setCancelable(false)
                .setNegativeButton("ok",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
}
