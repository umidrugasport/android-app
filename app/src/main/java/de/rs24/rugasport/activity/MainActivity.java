package de.rs24.rugasport.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import de.rs24.rugasport.R;
import de.rs24.rugasport.RugaSportApplication;
import de.rs24.rugasport.RugaSportDefaults;
import de.rs24.rugasport.activity.fragment.MenFragment;
import de.rs24.rugasport.activity.fragment.SearchFragment;
import de.rs24.rugasport.activity.fragment.WhatsNewFragment;
import de.rs24.rugasport.activity.fragment.WomenFragment;
import de.rs24.rugasport.api.RugaSportCallback;
import de.rs24.rugasport.cache.RugaSportCacheManager;
import de.rs24.rugasport.exceptions.ApiException;
import de.rs24.rugasport.model.category.CategoryModel;
import de.rs24.rugasport.model.product.ItemsModel;
import de.rs24.rugasport.model.product.ProductModel;
import de.rs24.rugasport.service.AdminService;
import de.rs24.rugasport.service.CategoryService;
import de.rs24.rugasport.service.SearchService;

import static de.rs24.rugasport.activity.Constants.CATEGORY_ID;
import static de.rs24.rugasport.activity.Constants.CATEGORY_NAME;
import static de.rs24.rugasport.activity.Constants.CURRENT_PAGE;
import static de.rs24.rugasport.activity.Constants.FRAGMENT_ARG;
import static de.rs24.rugasport.activity.Constants.MAIN_ACTIVITY;
import static de.rs24.rugasport.activity.Constants.MEN_CAT;
import static de.rs24.rugasport.activity.Constants.MEN_FRAGMENT;
import static de.rs24.rugasport.activity.Constants.PRODUCT_LIST;
import static de.rs24.rugasport.activity.Constants.PRODUCT_TABLE_ACTIVITY;
import static de.rs24.rugasport.activity.Constants.SEARCH_STRING;
import static de.rs24.rugasport.activity.Constants.WHATS_NEW_FRAGMENT;
import static de.rs24.rugasport.activity.Constants.WOMEN_CAT;
import static de.rs24.rugasport.activity.Constants.WOMEN_FRAGMENT;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @Inject
    AdminService adminService;

    @Inject
    SearchService searchService;

    @Inject
    CategoryService categoryService;

    @Inject
    RugaSportCacheManager cacheManager;

    NavigationView navigationView;

    Fragment fragment;

    ImageView searchBtn;
    EditText searchEditText;
    ProgressBar searchProgressBar;
    ProductModel[] productList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setTitle("");
        setSupportActionBar(toolbar);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        ((RugaSportApplication) getApplication()).getRugaSportComponent().inject(this);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                closeKeyboard();
                if(cacheManager.getAdminToken().equals("")){
                    appUpdateRequired();
                }
                Snackbar.make(view, R.string.empty_shopping_cart, Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        navigationView.getMenu().getItem(0).setChecked(true);

        /*
         *   GET ADMIN TOKEN
         */
        if(cacheManager.getAdminToken().equals("")){
            // get admin token
            adminService.getAdminToken(new RugaSportCallback<String>() {
                @Override
                public void onSuccess(String response) {
                    Toast.makeText(MainActivity.this, R.string.admin_token_generated, Toast.LENGTH_SHORT).show();
                    // get categories
                    categoryService.getCategories(new RugaSportCallback<CategoryModel>() {
                        @Override
                        public void onSuccess(CategoryModel response) {
                            Toast.makeText(MainActivity.this, R.string.categories_updated, Toast.LENGTH_SHORT).show();
                            Log.i(RugaSportDefaults.LOG_TAG,response.getName());

                            fragment = new WhatsNewFragment();
                            cacheManager.saveLastFragment(WHATS_NEW_FRAGMENT);
                            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                            ft.replace(R.id.content_main, fragment, "");
                            ft.commit();

                        }
                        @Override
                        public void onFailure(ApiException ex) {
                            cacheManager.deleteAdminToken();
                            handleApiError(ex);
                        }
                    });
                }
                @Override
                public void onFailure(ApiException ex) {
                    Log.i(RugaSportDefaults.LOG_TAG,ex.getMessage());
                    appUpdateRequired();
                }
            });
        }
    }

    public void handleApiError(ApiException ex) {
        Log.i(RugaSportDefaults.LOG_TAG,ex.getMessage());
        Toast.makeText(MainActivity.this, ex.getMessage(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        final MenuItem alertMenuItem = menu.findItem(R.id.search_menu_item);
        RelativeLayout rootView = (RelativeLayout) alertMenuItem.getActionView();
        searchEditText = (EditText) rootView.findViewById(R.id.search_et);
        searchProgressBar = (ProgressBar) rootView.findViewById(R.id.search_progress_bar);
        searchBtn = (ImageView) rootView.findViewById(R.id.search_iv);
        searchBtn.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            public void onClick(View v) {
                doSearch();
            }
        });
        return super.onPrepareOptionsMenu(menu);
    }

    private void doSearch() {
        productList = null;
        View view = (View) findViewById(R.id.content_main);
        InputMethodManager imm = (InputMethodManager)getSystemService(MainActivity.this.INPUT_METHOD_SERVICE);
        if(searchEditText.getVisibility() == View.INVISIBLE){
            searchBtn.setBackgroundColor(Color.GRAY);
            searchEditText.setVisibility(View.VISIBLE);
            searchEditText.setText("");
            searchEditText.requestFocus();
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
        }else if(!searchEditText.getText().toString().equals("")){
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            searchEditText.setVisibility(View.INVISIBLE);
            searchProgressBar.setVisibility(View.VISIBLE);
            searchBtn.setVisibility(View.INVISIBLE);
            searchService.quickSearch(searchEditText.getText().toString(), new RugaSportCallback<ItemsModel>() {
                @Override
                public void onSuccess(ItemsModel response) {
                    final int productAmount = response.getItems().length;
                    final int[] counter = {0};
                    if(productAmount > 0) {
                        productList = new ProductModel[productAmount];
                        ProductModel[] productItems = response.getItems();
                        for (ProductModel item : productItems) {
                            searchService.searchProductById(item.getId(), new RugaSportCallback<ItemsModel>() {
                                @Override
                                public void onSuccess(ItemsModel response) {
                                    productList[counter[0]] = response.getItems()[0];
                                    counter[0]++;
                                    if(counter[0] == productAmount){
                                        showSearchResult();
                                    }
                                }
                                @Override
                                public void onFailure(ApiException ex) {
                                }
                            });
                        }
                    }else{
                        showSearchResult();
                    }
                }
                @Override
                public void onFailure(ApiException ex) {
                }
            });
        }else{
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            searchBtn.setBackgroundResource(0);
            searchBtn.setVisibility(View.VISIBLE);
            searchProgressBar.setVisibility(View.INVISIBLE);
            searchEditText.setVisibility(View.INVISIBLE);
        }
    }

    private void showSearchResult() {
        searchBtn.setBackgroundResource(0);
        searchBtn.setVisibility(View.VISIBLE);
        searchProgressBar.setVisibility(View.INVISIBLE);
        fragment = new SearchFragment();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Bundle arguments = new Bundle();
        arguments.putString(SEARCH_STRING, searchEditText.getText().toString());
        if(productList != null && productList.length > 0){
            Gson gson = new Gson();
            arguments.putString(PRODUCT_LIST, gson.toJson(productList));
        }
        fragment.setArguments(arguments);
        ft.replace(R.id.content_main, fragment, "");
        ft.commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        fragment = null;
        closeKeyboard();
        if(cacheManager.getAdminToken().equals("")){
            appUpdateRequired();
        }else {

            int id = item.getItemId();

            if (id == R.id.nav_new) {
                fragment = new WhatsNewFragment();
                cacheManager.saveLastFragment(WHATS_NEW_FRAGMENT);
            } else if (id == R.id.nav_women) {
                fragment = new WomenFragment();
                Bundle arguments = new Bundle();
                arguments.putString(FRAGMENT_ARG, WOMEN_CAT);
                fragment.setArguments(arguments);
                cacheManager.saveLastFragment(WOMEN_FRAGMENT);
            } else if (id == R.id.nav_men) {
                fragment = new MenFragment();
                Bundle arguments = new Bundle();
                arguments.putString(FRAGMENT_ARG, MEN_CAT);
                fragment.setArguments(arguments);
                cacheManager.saveLastFragment(WOMEN_FRAGMENT);
            } else if (id == R.id.nav_kids) {

            } else if (id == R.id.nav_sports) {

            } else if (id == R.id.nav_brands) {

            } else if (id == R.id.nav_sale) {

            }
            if (fragment != null) {
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.content_main, fragment, "");
                ft.commit();
            }
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void appUpdateRequired() {
        Toast.makeText(MainActivity.this, R.string.app_update_required, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onStart() {
        super.onStart();
        setView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setView();
    }

    @Override
    protected void onStop() {
        super.onStop();
        closeKeyboard();
    }

    @Override
    protected void onPause() {
        super.onPause();
        closeKeyboard();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        closeKeyboard();
    }

    private void closeKeyboard() {
        View view = (View) findViewById(R.id.content_main);
        InputMethodManager imm = (InputMethodManager)getSystemService(MainActivity.this.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private void setView() {
        // fragment history
        String lastActivity = cacheManager.getLastActivity();
        String lastFragment = cacheManager.getLastFragment();
        if(!lastActivity.equals("")) {
            if(lastActivity.equals(MAIN_ACTIVITY)) {
                Bundle arguments;
                switch (lastFragment) {
                    case WOMEN_FRAGMENT:
                        navigationView.getMenu().getItem(1).setChecked(true);
                        fragment = new WomenFragment();
                        arguments = new Bundle();
                        arguments.putString(FRAGMENT_ARG, WOMEN_CAT);
                        fragment.setArguments(arguments);
                        break;
                    case MEN_FRAGMENT:
                        navigationView.getMenu().getItem(1).setChecked(true);
                        fragment = new WomenFragment();
                        arguments = new Bundle();
                        arguments.putString(FRAGMENT_ARG, MEN_CAT);
                        fragment.setArguments(arguments);
                        break;
                    default:
                        fragment = new WhatsNewFragment();
                        cacheManager.saveLastFragment(WHATS_NEW_FRAGMENT);
                        break;
                }
            }else if(lastActivity.equals(PRODUCT_TABLE_ACTIVITY)){
                Intent intent = new Intent(MainActivity.this, ProductTableActivity.class);
                intent.putExtra(CATEGORY_ID, cacheManager.getLastCatId());
                intent.putExtra(CATEGORY_NAME, cacheManager.getLastCatName());
                intent.putExtra(CURRENT_PAGE, cacheManager.getLastPage());
                this.startActivity(intent);
            }else{
                cacheManager.saveLastActivity(MAIN_ACTIVITY);
                fragment = new WhatsNewFragment();
                cacheManager.saveLastFragment(WHATS_NEW_FRAGMENT);
            }
        }else{
            cacheManager.saveLastActivity(MAIN_ACTIVITY);
            fragment = new WhatsNewFragment();
            cacheManager.saveLastFragment(WHATS_NEW_FRAGMENT);
        }
        if (fragment != null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.content_main, fragment, "");
            ft.commit();
        }
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        super.dispatchKeyEvent(event);
        try
        {
            if(event.getAction() == KeyEvent.ACTION_UP && event.getKeyCode() == 66)
            {
                if(searchBtn.getBackground()!=null && !searchEditText.getText().toString().equals("")){
                    doSearch();
                }
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
        return true;
    }
}
