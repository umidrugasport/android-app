package de.rs24.rugasport.activity;

public class Constants {
    public static final String FRAGMENT_TAG = "FRAGMENT_TAG";
    public static final String FRAGMENT_ARG = "FRAGMENT_ARG";
    public static final String SEARCH_STRING = "SEARCH_STRING";
    public static final String PRODUCT_LIST = "PRODUCT_LIST";
    public static final String CATEGORY_ID = "CATEGORY_ID";
    public static final String CATEGORY_NAME = "CATEGORY_NAME";
    public static final String CURRENT_PAGE = "CURRENT_PAGE";
    public static final String MAIN_ACTIVITY = "MainActivity";
    public static final String PRODUCT_TABLE_ACTIVITY = "ProductTableActivity";
    public static final String WOMEN_FRAGMENT = "WomenFragment";
    public static final String MEN_FRAGMENT = "MenFragment";
    public static final String WHATS_NEW_FRAGMENT = "WhatsNewFragment";
    public static final String WOMEN_CAT = "Women";
    public static final String WOMEN_SUB_CAT1 = "Tops";
    public static final String WOMEN_SUB_CAT2 = "Bottoms";
    public static final String MEN_CAT = "Men";
    public static final String LIST_TAG  = "LIST_TAG";
    public static final String ITEM_SKU  = "ITEM_SKU";
    public static final String ITEM_NAME  = "ITEM_NAME";
    public static final String ITEM_PRICE  = "ITEM_PRICE";
    public static final String ITEM_IMAGE_URL  = "ITEM_IMAGE_URL";
    public static final String ITEM_DESCRIPTION  = "ITEM_DESCRIPTION";
    public static final String ITEM_IS_SALE  = "ITEM_IS_SALE";
    public static final String ITEM_POSITION = "ITEM_POSITION";
}
