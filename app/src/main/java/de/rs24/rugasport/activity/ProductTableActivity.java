package de.rs24.rugasport.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import de.rs24.rugasport.BuildConfig;
import de.rs24.rugasport.R;
import de.rs24.rugasport.RugaSportApplication;
import de.rs24.rugasport.activity.adapter.ProductListAdapter;
import de.rs24.rugasport.api.RugaSportCallback;
import de.rs24.rugasport.cache.RugaSportCacheManager;
import de.rs24.rugasport.exceptions.ApiException;
import de.rs24.rugasport.model.listModel.ProductItemModel;
import de.rs24.rugasport.model.product.ItemsModel;
import de.rs24.rugasport.model.product.ProductModel;
import de.rs24.rugasport.service.ProductService;

import static de.rs24.rugasport.activity.Constants.CATEGORY_ID;
import static de.rs24.rugasport.activity.Constants.CATEGORY_NAME;
import static de.rs24.rugasport.activity.Constants.CURRENT_PAGE;
import static de.rs24.rugasport.activity.Constants.MAIN_ACTIVITY;

public class ProductTableActivity extends AppCompatActivity {

    @Inject
    RugaSportCacheManager cacheManager;

    @Inject
    ProductService productService;

    private String categoryName = "";
    private int categoryId  = 1;
    private int currentPage = 1;

    private List<Integer> pItemLayouts = new ArrayList<Integer>();
    RecyclerView pListRecyclerView;
    ProductListAdapter pListAdapter;
    List<ProductItemModel> pList;
    private String pListTask = "";
    private ProductItemModel pListModel;

    private TextView    pageNumberTv;
    private ImageButton forwardBtn;
    private ImageButton backBtn;

    private ImageView homeBtn;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        cacheManager.saveLastActivity(MAIN_ACTIVITY);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_table);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ((RugaSportApplication) getApplication()).getRugaSportComponent().inject(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            if(extras.getString(CATEGORY_NAME) != null){
                categoryName = extras.getString(CATEGORY_NAME);
            }
            if(extras.getString(CATEGORY_ID) != null){
                categoryId = Integer.parseInt(extras.getString(CATEGORY_ID));
            }
            if(extras.getString(CURRENT_PAGE) != null){
                currentPage = Integer.parseInt(extras.getString(CURRENT_PAGE));
            }
        }

        cacheManager.saveLastCatId(String.valueOf(categoryId));
        cacheManager.saveLastCatName(categoryName);
        cacheManager.saveLastPage(String.valueOf(currentPage));

        getSupportActionBar().setTitle(categoryName);

        pageNumberTv = (TextView) findViewById(R.id.tv_page_number);
        pageNumberTv.setText(currentPage+"");

        forwardBtn  = (ImageButton) findViewById(R.id.forward_btn);
        forwardBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                currentPage = currentPage + 1;
                Intent intent = new Intent(ProductTableActivity.this, ProductTableActivity.class);
                intent.putExtra(CATEGORY_ID, categoryId+"");
                intent.putExtra(CATEGORY_NAME, categoryName);
                intent.putExtra(CURRENT_PAGE, currentPage+"");
                ProductTableActivity.this.startActivity(intent);
            }
        });

        backBtn = (ImageButton) findViewById(R.id.back_btn);
        backBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                currentPage = currentPage - 1;
                Intent intent = new Intent(ProductTableActivity.this, ProductTableActivity.class);
                intent.putExtra(CATEGORY_ID, categoryId+"");
                intent.putExtra(CATEGORY_NAME, categoryName);
                intent.putExtra(CURRENT_PAGE, currentPage+"");
                ProductTableActivity.this.startActivity(intent);
            }
        });
        if(currentPage == 1){
            backBtn.setVisibility(View.INVISIBLE);
        }else{
            backBtn.setVisibility(View.VISIBLE);
        }

        homeBtn = (ImageView) findViewById(R.id.home_btn);
        homeBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(ProductTableActivity.this, MainActivity.class);
                cacheManager.saveLastActivity(MAIN_ACTIVITY);
                cacheManager.saveLastFragment("");
                cacheManager.saveLastPage("1");
                cacheManager.saveLastCatName("");
                cacheManager.saveLastCatId("1");
                ProductTableActivity.this.startActivity(intent);
            }
        });

        productService.getTopProducts(categoryId, 10, currentPage, new RugaSportCallback<ItemsModel>() {
            @Override
            public void onSuccess(ItemsModel response) {
                pList = new ArrayList<>();
                pListAdapter = new ProductListAdapter(ProductTableActivity.this, pList);
                pListAdapter.setListTag(pListTask);
                pListRecyclerView = (RecyclerView) findViewById(R.id.product_list_recycler_view);
                ProductModel[] productItems = response.getItems();
                if(productItems.length <10){
                    forwardBtn.setVisibility(View.INVISIBLE);
                }else{
                    forwardBtn.setVisibility(View.VISIBLE);
                }
                for (ProductModel product : productItems) {
                    if( product.getPrice() != 0 ) {
                        pListModel = new ProductItemModel();
                        pListModel.setSku(product.getSku());
                        pListModel.setName(product.getName());
                        pListModel.setPrice(product.getPrice() + ",00 €");
                        JsonArray attributes = product.getCustom_attributes();
                        for (JsonElement attribute : attributes) {
                            JsonObject attr = attribute.getAsJsonObject();
                            if (attr.get("attribute_code").getAsString().equals("image")) {
                                pListModel.setImageUrl(BuildConfig.BASE_URL + BuildConfig.IMAGE_PATH + attr.get("value").getAsString());
                            }
                            if (attr.get("attribute_code").getAsString().equals("description")) {
                                pListModel.setDescription(attr.get("value").getAsString());
                            }
                        }
                        pItemLayouts.add(R.layout.list_product_item_hot_seller);
                        pList.add(pListModel);
                    }
                }
                pListAdapter.setItemLayouts(pItemLayouts);
                pListRecyclerView.setAdapter(pListAdapter);
            }
            @Override
            public void onFailure(ApiException ex) {

            }
        });

    }

}
