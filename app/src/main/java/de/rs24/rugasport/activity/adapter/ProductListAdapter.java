package de.rs24.rugasport.activity.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import de.rs24.rugasport.R;
import de.rs24.rugasport.RugaSportApplication;
import de.rs24.rugasport.activity.ProductActivity;
import de.rs24.rugasport.cache.RugaSportCacheManager;
import de.rs24.rugasport.model.listModel.ProductItemModel;

import static de.rs24.rugasport.activity.Constants.FRAGMENT_ARG;
import static de.rs24.rugasport.activity.Constants.ITEM_DESCRIPTION;
import static de.rs24.rugasport.activity.Constants.ITEM_IMAGE_URL;
import static de.rs24.rugasport.activity.Constants.ITEM_IS_SALE;
import static de.rs24.rugasport.activity.Constants.ITEM_NAME;
import static de.rs24.rugasport.activity.Constants.ITEM_PRICE;
import static de.rs24.rugasport.activity.Constants.ITEM_SKU;

public class ProductListAdapter extends RecyclerView.Adapter<ProductListAdapter.ViewHolder>{

    private List<ProductItemModel> mItems;
    private Context mContext;
    private int itemLayout;

    private int itemIndex = 0;
    private String listTag = "";
    private List<Integer> itemLayouts = new ArrayList<Integer>();

    public ProductListAdapter(Context context, List<ProductItemModel> items) {
        this.mContext = context;
        this.mItems = items;
    }

    public void setItemLayout(int itemLayout){
        this.itemLayout = itemLayout;
    }

    public void setListTag(String listTag){
        this.listTag = listTag;
    }

    @Override
    public void onViewAttachedToWindow(ProductListAdapter.ViewHolder holder) {
        super.onViewAttachedToWindow(holder);

        Activity activity = (Activity) mContext;
        ViewGroup viewGroup = (ViewGroup) ((ViewGroup) activity.findViewById(android.R.id.content)).getChildAt(0);

        RecyclerView recyclerView = (RecyclerView) viewGroup.findViewById(R.id.product_list_recycler_view);
    }

    @Override
    public ProductListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        this.itemLayout = itemLayouts.get(itemIndex);
        itemIndex++;
        View itemView = inflater.inflate(itemLayout, parent, false);
        ProductListAdapter.ViewHolder viewHolder = new ProductListAdapter.ViewHolder(mContext, itemView, itemLayout);
        return viewHolder;
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBindViewHolder(ProductListAdapter.ViewHolder holder, int position) {

        final ProductItemModel item = mItems.get(position);

        holder.itemSku.setText(item.getSku());
        holder.itemName.setText(item.getName());
        holder.itemPrice.setText(item.getPrice());
        holder.itemDescription = item.getDescription();
        holder.itemIsSale = item.getSale();
        if(item.getImageUrl()!=null) {
            holder.itemImageUrl = item.getImageUrl();
            Picasso.with(mContext).load(item.getImageUrl()).into(holder.itemImage);
        }
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public void setItemLayouts( List<Integer> itemLayouts) {
        this.itemLayouts = itemLayouts;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @Inject
        RugaSportCacheManager cacheManager;

        public TextView itemSku;
        public TextView itemName;
        public TextView itemPrice;
        public ImageView itemImage;
        public String itemImageUrl = null;
        public String itemDescription = "";
        public Boolean itemIsSale;
        public View mView;
        public View itemLine;
        public RelativeLayout itemRelativeLayout;
        public Context mContext;
        public Activity activity;
        public ViewGroup viewGroup;

        public ViewHolder(final Context mContext, View itemView, int itemLayout) {
            super(itemView);
            this.mContext = mContext;

            activity = (Activity) mContext;
            ((RugaSportApplication) activity.getApplication()).getRugaSportComponent().inject(this);
            viewGroup = (ViewGroup) ((ViewGroup) activity.findViewById(android.R.id.content)).getChildAt(0);

            itemRelativeLayout = (RelativeLayout) itemView.findViewById(R.id.item_view);
            itemLine = (View) itemView.findViewById(R.id.item_line);
            switch (itemLayout) {
                case R.layout.list_product:
                case R.layout.list_product_item_hot_seller:
                    itemRelativeLayout.setOnClickListener(this);
                    itemSku     = (TextView) itemView.findViewById(R.id.item_sku);
                    itemName    = (TextView) itemView.findViewById(R.id.item_name);
                    itemPrice   = (TextView) itemView.findViewById(R.id.item_price);
                    itemImage   = (ImageView) itemView.findViewById(R.id.item_image);
                    break;
                default:
                    break;
            }
            mView = itemView;
        }

        public ViewHolder(View itemView) {
            super(itemView);
        }

        @Override
        public void onClick(View v) {

            activity = (Activity) mContext;

            ((RugaSportApplication) activity.getApplication()).getRugaSportComponent().inject(this);

            viewGroup = (ViewGroup) ((ViewGroup) activity.findViewById(android.R.id.content)).getChildAt(0);

            Intent intent;
            String itemPosition = String.valueOf(getAdapterPosition());

            String nextListTag = null;
            switch (listTag) {
                default:
                    intent = new Intent(mContext, ProductActivity.class);
                    intent.putExtra(ITEM_SKU, itemSku.getText());
                    intent.putExtra(ITEM_NAME, itemName.getText());
                    intent.putExtra(ITEM_PRICE, itemPrice.getText());
                    intent.putExtra(ITEM_IMAGE_URL, itemImageUrl);
                    intent.putExtra(ITEM_DESCRIPTION, itemDescription);
                    intent.putExtra(ITEM_IS_SALE, itemIsSale.toString());
                    mContext.startActivity(intent);
                    break;
            }
        }

        private void attachFragment(Fragment fragment, String tag, String extra) {
            FragmentTransaction ft = ((FragmentActivity) mContext).getSupportFragmentManager().beginTransaction();
            Bundle bundle = new Bundle();
            bundle.putString(FRAGMENT_ARG, extra);
            fragment.setArguments(bundle);
            ft.replace(R.id.content_main, fragment, tag);
            ft.commit();
        }
    }

    private void showSimpleDialog(String title, String message) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
        alertDialogBuilder.setTitle(title);
        alertDialogBuilder
                .setMessage(message)
                .setCancelable(false)
                .setNegativeButton("ok",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
}
