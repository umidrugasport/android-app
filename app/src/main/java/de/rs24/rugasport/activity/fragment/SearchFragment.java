package de.rs24.rugasport.activity.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

import de.rs24.rugasport.BuildConfig;
import de.rs24.rugasport.R;
import de.rs24.rugasport.RugaSportApplication;
import de.rs24.rugasport.activity.adapter.ProductListAdapter;
import de.rs24.rugasport.model.listModel.ProductItemModel;
import de.rs24.rugasport.model.product.ProductModel;

import static de.rs24.rugasport.activity.Constants.FRAGMENT_ARG;
import static de.rs24.rugasport.activity.Constants.PRODUCT_LIST;
import static de.rs24.rugasport.activity.Constants.SEARCH_STRING;

public class SearchFragment  extends Fragment {

    private String searchString = "";
    private TextView title;
    private TextView notFoundTv;
    private TextView copyRightTv;
    private TextView vatTv;

    private ProductModel[] productList;

    private List<Integer> pItemLayouts = new ArrayList<Integer>();
    RecyclerView pListRecyclerView;
    ProductListAdapter pListAdapter;
    List<ProductItemModel> pList;
    private String pListTask = "";
    private ProductItemModel pListModel;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_search, container, false);
        Activity activity = getActivity();

        notFoundTv = (TextView) view.findViewById(R.id.search_not_found);

        if (getArguments() != null) {
            Bundle arguments = getArguments();
            searchString = arguments.getString(SEARCH_STRING);
            Gson gson = new Gson();
            productList = (ProductModel[]) gson.fromJson(arguments.getString(PRODUCT_LIST), ProductModel[].class);
            title = (TextView) view.findViewById(R.id.search_title);
            title.setText( getString(R.string.search_result_for) + " " + searchString);
        }

        if(productList != null && productList.length > 0) {
            notFoundTv.setVisibility(View.GONE);
            copyRightTv = (TextView) view.findViewById(R.id.tv_copyright);
            copyRightTv.setVisibility(View.VISIBLE);
            vatTv = (TextView) view.findViewById(R.id.tv_vat);
            vatTv.setVisibility(View.VISIBLE);
            pList = new ArrayList<>();
            pListAdapter = new ProductListAdapter(getActivity(), pList);
            pListAdapter.setListTag(pListTask);
            pListRecyclerView = (RecyclerView) view.findViewById(R.id.product_list_recycler_view);
            for (ProductModel product : productList) {
                if(product != null)
                        pListModel = new ProductItemModel();
                        pListModel.setSku(product.getSku());
                        pListModel.setName(product.getName());
                        pListModel.setPrice(product.getPrice() + ",00 €"); // TODO
                        pListModel.setSale(true);
                        JsonArray attributes = product.getCustom_attributes();
                        for (JsonElement attribute : attributes) {
                            JsonObject attr = attribute.getAsJsonObject();
                            if (attr.get("attribute_code").getAsString().equals("image")) {
                                pListModel.setImageUrl(BuildConfig.BASE_URL + BuildConfig.IMAGE_PATH + attr.get("value").getAsString());
                            }
                            if (attr.get("attribute_code").getAsString().equals("description")) {
                                pListModel.setDescription(attr.get("value").getAsString());
                            }
                        }
                        pItemLayouts.add(R.layout.list_product_item_hot_seller);
                        pList.add(pListModel);
            }
            pListAdapter.setItemLayouts(pItemLayouts);
            pListRecyclerView.setAdapter(pListAdapter);
        }else{
            notFoundTv.setVisibility(View.VISIBLE);
        }

        return view;
    }
}
