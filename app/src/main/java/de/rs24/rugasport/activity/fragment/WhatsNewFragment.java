package de.rs24.rugasport.activity.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.HashMap;

import javax.inject.Inject;

import de.rs24.rugasport.BuildConfig;
import de.rs24.rugasport.R;
import de.rs24.rugasport.RugaSportApplication;
import de.rs24.rugasport.activity.MainActivity;
import de.rs24.rugasport.api.RugaSportCallback;
import de.rs24.rugasport.cache.RugaSportCacheManager;
import de.rs24.rugasport.exceptions.ApiException;
import de.rs24.rugasport.model.product.ItemsModel;
import de.rs24.rugasport.model.product.ProductModel;
import de.rs24.rugasport.service.ProductService;

public class WhatsNewFragment extends Fragment implements BaseSliderView.OnSliderClickListener,
        ViewPagerEx.OnPageChangeListener{

    @Inject
    RugaSportCacheManager cacheManager;

    @Inject
    ProductService productService;

    TextView title;

    WhatsNewFragment context;
    SliderLayout sliderLayout;
    HashMap<String, String> HashMapForURL;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_whatsnew, container, false);
        Activity activity = getActivity();
        ((RugaSportApplication) activity.getApplication()).getRugaSportComponent().inject(this);
        context = this;
        title = (TextView) view.findViewById(R.id.whatsnew_title);
        title.setText(R.string.whats_new);
        title.setVisibility(View.GONE);
        sliderLayout = (SliderLayout) view.findViewById(R.id.slider);
        sliderLayout.setVisibility(View.GONE);
        HashMapForURL = new HashMap<String, String>();
        // TODO: get whats new
        productService.getTopProducts(5, 3, 1, new RugaSportCallback<ItemsModel>() {
            @Override
            public void onSuccess(ItemsModel response) {
                ProductModel[] productItems = response.getItems();
                if(productItems != null && productItems.length > 0) {
                    for (ProductModel product : productItems) {
                        if (product.getPrice() != 0) {
                            JsonArray attributes = product.getCustom_attributes();
                            for (JsonElement attribute : attributes) {
                                JsonObject attr = attribute.getAsJsonObject();
                                if (attr.get("attribute_code").getAsString().equals("image")) {
                                    HashMapForURL.put(product.getName(), BuildConfig.BASE_URL + BuildConfig.IMAGE_PATH + attr.get("value").getAsString());
                                }
                            }
                        }
                    }

                }
                for(String name : HashMapForURL.keySet()){
                    TextSliderView textSliderView = new TextSliderView(getActivity());
                    textSliderView
                            .description(name)
                            .image(HashMapForURL.get(name))
                            .setScaleType(BaseSliderView.ScaleType.Fit)
                            .setOnSliderClickListener(context);
                    textSliderView.bundle(new Bundle());
                    textSliderView.getBundle()
                            .putString("extra",name);
                    sliderLayout.addSlider(textSliderView);
                }
                sliderLayout.setPresetTransformer(SliderLayout.Transformer.DepthPage);
                sliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
                sliderLayout.setCustomAnimation(new DescriptionAnimation());
                sliderLayout.setDuration(5000);
                sliderLayout.addOnPageChangeListener(context);
                title.setVisibility(View.VISIBLE);
                sliderLayout.setVisibility(View.VISIBLE);
            }
            @Override
            public void onFailure(ApiException ex) {
                ((MainActivity) getActivity()).handleApiError(ex);
            }
        });

        return view;
    }

    @Override
    public void onSliderClick(BaseSliderView slider) {
        Toast.makeText(getActivity(),slider.getBundle().get("extra") + "", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
