package de.rs24.rugasport.activity.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import de.rs24.rugasport.BuildConfig;
import de.rs24.rugasport.R;
import de.rs24.rugasport.RugaSportApplication;
import de.rs24.rugasport.activity.MainActivity;
import de.rs24.rugasport.activity.adapter.MenuListAdapter;
import de.rs24.rugasport.activity.adapter.ProductListAdapter;
import de.rs24.rugasport.api.RugaSportCallback;
import de.rs24.rugasport.cache.RugaSportCacheManager;
import de.rs24.rugasport.exceptions.ApiException;
import de.rs24.rugasport.model.category.CategoryModel;
import de.rs24.rugasport.model.listModel.ListItemModel;
import de.rs24.rugasport.model.listModel.ProductItemModel;
import de.rs24.rugasport.model.product.ItemsModel;
import de.rs24.rugasport.model.product.ProductModel;
import de.rs24.rugasport.service.ProductService;

import static de.rs24.rugasport.activity.Constants.FRAGMENT_ARG;
import static de.rs24.rugasport.activity.Constants.MEN_CAT;
import static de.rs24.rugasport.activity.Constants.WOMEN_CAT;

public class MenFragment  extends Fragment {

    @Inject
    RugaSportCacheManager cacheManager;

    @Inject
    ProductService productService;

    private List<Integer> itemLayouts = new ArrayList<Integer>();
    RecyclerView listRecyclerView;
    MenuListAdapter listAdapter;
    List<ListItemModel> list;
    private String listTask;
    private ListItemModel listModel;

    private List<Integer> pItemLayouts = new ArrayList<Integer>();
    RecyclerView pListRecyclerView;
    ProductListAdapter pListAdapter;
    List<ProductItemModel> pList;
    private String pListTask = "";
    private ProductItemModel pListModel;

    TextView title;
    TextView hotSales;
    private int catId;
    ScrollView scrollView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_women, container, false);
        scrollView = (ScrollView) view.findViewById(R.id.women_scrollview);
        scrollView.setVisibility(View.VISIBLE);
        Activity activity =  getActivity();
        ((RugaSportApplication) activity.getApplication()).getRugaSportComponent().inject(this);

        if (getArguments() != null) {
            Bundle arguments = getArguments();
            listTask = arguments.getString(FRAGMENT_ARG);
        }

        title = (TextView) view.findViewById(R.id.women_title);
        title.setText(R.string.men);

        list = new ArrayList<>();
        listAdapter = new MenuListAdapter(getActivity(), list);
        listAdapter.setListTag(listTask);
        listRecyclerView = (RecyclerView) view.findViewById(R.id.menu_list_recycler_view);

        CategoryModel[] cModel = null;
        CategoryModel[] categoryModel = (CategoryModel[]) cacheManager.getCategories().getChildren_data();
        for (CategoryModel catModel : categoryModel) {
            if (catModel.getName().equals(MEN_CAT)) {
                cModel = (CategoryModel[]) catModel.getChildren_data();
                catId = 17; // TODO: catModel.getId();
            }
        }
        if(listTask.equals(WOMEN_CAT)) {
            for (CategoryModel cm : cModel) {
                listModel = new ListItemModel();
                listModel.setKey(cm.getName());
                itemLayouts.add(R.layout.list_menu_item);
                list.add(listModel);
            }
        } else {
            for (CategoryModel cm : cModel) {
                listModel = new ListItemModel();
                listModel.setKey(cm.getName());
                itemLayouts.add(R.layout.list_menu_item);
                if(cm.getName().equals(listTask))
                    listModel.setArrow(R.drawable.ic_keyboard_arrow_up);
                list.add(listModel);
                if(cm.getName().equals(listTask))
                    for (CategoryModel c : (CategoryModel[]) cm.getChildren_data()){
                        listModel = new ListItemModel();
                        listModel.setKey(c.getName());
                        listModel.setValue(""+c.getProduct_count());
                        listModel.setCatId(c.getId());
                        listModel.setCatName(c.getName());
                        itemLayouts.add(R.layout.list_menu_sub_item);
                        list.add(listModel);
                    }
            }
        }

        //listAdapter.setItemLayouts(itemLayouts);
        //listRecyclerView.setAdapter(listAdapter);

        // TODO: get top sellers
        productService.getTopProducts(catId, 5, 10, new RugaSportCallback<ItemsModel>() {
            @Override
            public void onSuccess(ItemsModel response) {
                ProductModel[] productItems = response.getItems();
                if(productItems != null && productItems.length > 0) {
                    hotSales = (TextView) view.findViewById(R.id.search_title);
                    hotSales.setVisibility(View.VISIBLE);
                    pList = new ArrayList<>();
                    pListAdapter = new ProductListAdapter(getActivity(), pList);
                    pListAdapter.setListTag(pListTask);
                    pListRecyclerView = (RecyclerView) view.findViewById(R.id.product_list_recycler_view);
                    for (ProductModel product : productItems) {
                        if (product.getPrice() != 0) {
                            pListModel = new ProductItemModel();
                            pListModel.setSku(product.getSku());
                            pListModel.setName(product.getName());
                            pListModel.setPrice(product.getPrice() + ",00 €");
                            pListModel.setSale(true);
                            JsonArray attributes = product.getCustom_attributes();
                            for (JsonElement attribute : attributes) {
                                JsonObject attr = attribute.getAsJsonObject();
                                if (attr.get("attribute_code").getAsString().equals("image")) {
                                    pListModel.setImageUrl(BuildConfig.BASE_URL + BuildConfig.IMAGE_PATH + attr.get("value").getAsString());
                                }
                                if (attr.get("attribute_code").getAsString().equals("description")) {
                                    pListModel.setDescription(attr.get("value").getAsString());
                                }
                            }
                            pItemLayouts.add(R.layout.list_product_item_hot_seller);
                            pList.add(pListModel);
                        }
                    }
                    pListAdapter.setItemLayouts(pItemLayouts);
                    pListRecyclerView.setAdapter(pListAdapter);
                }
            }
            @Override
            public void onFailure(ApiException ex) {
                ((MainActivity) getActivity()).handleApiError(ex);
            }
        });
        return view;
    }
}