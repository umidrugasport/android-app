package de.rs24.rugasport.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import de.rs24.rugasport.R;

import static de.rs24.rugasport.activity.Constants.ITEM_DESCRIPTION;
import static de.rs24.rugasport.activity.Constants.ITEM_IMAGE_URL;
import static de.rs24.rugasport.activity.Constants.ITEM_IS_SALE;
import static de.rs24.rugasport.activity.Constants.ITEM_NAME;
import static de.rs24.rugasport.activity.Constants.ITEM_PRICE;
import static de.rs24.rugasport.activity.Constants.ITEM_SKU;

public class ProductActivity extends AppCompatActivity {

    private String sku          = "";
    private String name         = "";
    private String price        = "";
    private String description  = "";
    private String imageUrl     = null;
    private Boolean isSale      = false;

    private TextView    skuTv;
    private TextView    nameTv;
    private TextView    priceTv;
    private TextView    descriptionTv;
    private TextView    isSaleTv;

    private ImageView   imageView;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            sku         = extras.getString(ITEM_SKU);
            name        = extras.getString(ITEM_NAME);
            price       = extras.getString(ITEM_PRICE);
            description = extras.getString(ITEM_DESCRIPTION);
            imageUrl    = extras.getString(ITEM_IMAGE_URL);
            isSale      = Boolean.valueOf(extras.getString(ITEM_IS_SALE));
        }

        getSupportActionBar().setTitle(name);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        imageView   = (ImageView)   findViewById(R.id.item_image);
        Picasso.with(this).load(imageUrl).into(imageView);

        skuTv           = (TextView)    findViewById(R.id.item_sku);
        nameTv          = (TextView)    findViewById(R.id.item_name);
        priceTv         = (TextView)    findViewById(R.id.item_price);
        isSaleTv        = (TextView)    findViewById(R.id.item_sale);
        descriptionTv   = (TextView)    findViewById(R.id.item_description);

        skuTv.setText("SKU#: "+sku);
        nameTv.setText(name);
        priceTv.setText(price);
        descriptionTv.setText(Html.fromHtml(description));

        isSaleTv.setVisibility(View.INVISIBLE);
        if(isSale){
            isSaleTv.setVisibility(View.VISIBLE);
            priceTv.setTextColor(Color.BLACK);
        }

    }

}
