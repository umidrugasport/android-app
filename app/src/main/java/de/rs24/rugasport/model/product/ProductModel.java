package de.rs24.rugasport.model.product;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.JsonArray;
import com.google.gson.annotations.SerializedName;

public class ProductModel implements Parcelable {

    @SerializedName("id")
    private int id;

    @SerializedName("sku")
    private String sku;

    @SerializedName("price")
    private int price;

    @SerializedName("name")
    private String name;

    @SerializedName("custom_attributes")
    private JsonArray custom_attributes;

    public ProductModel() {
    }

    public ProductModel(int id, String sku, int price, String name, JsonArray custom_attributes) {
        this.id = id;
        this.sku = sku;
        this.price = price;
        this.name = name;
        this.custom_attributes = custom_attributes;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public JsonArray getCustom_attributes() {
        return custom_attributes;
    }

    public void setCustom_attributes(JsonArray custom_attributes) {
        this.custom_attributes = custom_attributes;
    }

    public static Creator<ProductModel> getCREATOR() {
        return CREATOR;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.sku);
        dest.writeInt(this.price);
        dest.writeString(this.name);
        dest.writeString(this.custom_attributes.toString());
    }

    protected ProductModel(Parcel in) {
        this.id = in.readInt();
        this.sku = in.readString();
        this.price = in.readInt();
        this.name = in.readString();
        this.custom_attributes = in.readParcelable(JsonArray.class.getClassLoader());
    }

    public static final Creator<ProductModel> CREATOR = new Creator<ProductModel>() {
        @Override
        public ProductModel createFromParcel(Parcel source) {
            return new ProductModel(source);
        }

        @Override
        public ProductModel[] newArray(int size) {
            return new ProductModel[size];
        }
    };
}
