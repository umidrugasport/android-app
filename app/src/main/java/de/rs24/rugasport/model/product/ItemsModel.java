package de.rs24.rugasport.model.product;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class ItemsModel implements Parcelable {

    @SerializedName("items")
    private ProductModel[] items;

    public ItemsModel() {
    }

    public ItemsModel(ProductModel[] items) {
        this.items = items;
    }

    public ProductModel[] getItems() {
        return items;
    }

    public void setItems(ProductModel[] items) {
        this.items = items;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedArray(this.items, flags);
    }

    protected ItemsModel(Parcel in) {
        this.items = in.createTypedArray(ProductModel.CREATOR);
    }

    public static final Parcelable.Creator<ItemsModel> CREATOR = new Parcelable.Creator<ItemsModel>() {
        @Override
        public ItemsModel createFromParcel(Parcel source) {
            return new ItemsModel(source);
        }

        @Override
        public ItemsModel[] newArray(int size) {
            return new ItemsModel[size];
        }
    };
}
