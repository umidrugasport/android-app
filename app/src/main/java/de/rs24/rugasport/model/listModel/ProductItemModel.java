package de.rs24.rugasport.model.listModel;

import android.os.Parcel;
import android.os.Parcelable;

public class ProductItemModel implements Parcelable {

    private int id;
    private String sku;
    private String name;
    private String price;
    private String description;
    private String imageUrl;
    private Boolean isSale = false;

    public ProductItemModel() {
    }

    public ProductItemModel(int id, String sku, String name, String price, String description, String imageUrl, Boolean isSale) {
        this.id = id;
        this.sku = sku;
        this.name = name;
        this.price = price;
        this.description = description;
        this.imageUrl = imageUrl;
        this.isSale = isSale;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public Boolean getSale() {
        return isSale;
    }

    public void setSale(Boolean sale) {
        isSale = sale;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.sku);
        dest.writeString(this.name);
        dest.writeString(this.price);
        dest.writeString(this.description);
        dest.writeString(this.imageUrl);
        dest.writeValue(this.isSale);
    }

    protected ProductItemModel(Parcel in) {
        this.id = in.readInt();
        this.sku = in.readString();
        this.name = in.readString();
        this.price = in.readString();
        this.description = in.readString();
        this.imageUrl = in.readString();
        this.isSale = (Boolean) in.readValue(Boolean.class.getClassLoader());
    }

    public static final Creator<ProductItemModel> CREATOR = new Creator<ProductItemModel>() {
        @Override
        public ProductItemModel createFromParcel(Parcel source) {
            return new ProductItemModel(source);
        }

        @Override
        public ProductItemModel[] newArray(int size) {
            return new ProductItemModel[size];
        }
    };
}
