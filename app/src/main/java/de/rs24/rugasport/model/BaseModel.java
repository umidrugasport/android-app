package de.rs24.rugasport.model;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import java.util.Date;

import javax.inject.Inject;

public class BaseModel implements Parcelable {

    @Inject
    protected Context context;

    @SerializedName("message")
    private String message = "";

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    protected Date getDate(String dateValue, String pattern) {

        if (TextUtils.isEmpty(dateValue)) {
            return null;
        }

        try {
            return DateTime.parse(dateValue, DateTimeFormat.forPattern(pattern)).toDate();
        } catch (Exception ex) {
            return null;
        }
    }

    public BaseModel() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.message);
    }

    protected BaseModel(Parcel in) {
        this.context = in.readParcelable(Context.class.getClassLoader());
        this.message = in.readString();
    }

    public static final Creator<BaseModel> CREATOR = new Creator<BaseModel>() {
        @Override
        public BaseModel createFromParcel(Parcel in) {
            return new BaseModel(in);
        }

        @Override
        public BaseModel[] newArray(int size) {
            return new BaseModel[size];
        }
    };
}

