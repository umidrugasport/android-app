package de.rs24.rugasport.model.category;

import android.os.Parcel;

import com.google.gson.annotations.SerializedName;

public class CategoryModel extends CategoryBaseModel {

    @SerializedName("children_data")
    private CategoryModel[] children_data;

    public CategoryModel() {
    }

    public CategoryModel(CategoryModel[] children_data) {
        this.children_data = children_data;
    }

    public CategoryBaseModel[] getChildren_data() {
        return children_data;
    }

    public void setChildren_data(CategoryModel[] children_data) {
        this.children_data = children_data;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeTypedArray(this.children_data, flags);
    }

    protected CategoryModel(Parcel in) {
        super(in);
        this.children_data = in.createTypedArray(CategoryModel.CREATOR);
    }

    public static final Creator<CategoryModel> CREATOR = new Creator<CategoryModel>() {
        @Override
        public CategoryModel createFromParcel(Parcel source) {
            return new CategoryModel(source);
        }

        @Override
        public CategoryModel[] newArray(int size) {
            return new CategoryModel[size];
        }
    };
}
