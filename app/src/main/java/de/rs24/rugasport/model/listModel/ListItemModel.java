package de.rs24.rugasport.model.listModel;

import android.os.Parcel;
import android.os.Parcelable;

public class ListItemModel implements Parcelable {

    private int id;
    private String key;
    private String value;
    private String image;
    private int arrow;
    private int catId;
    private String catName;

    public ListItemModel() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getArrow() {
        return arrow;
    }

    public void setArrow(int arrow) {
        this.arrow = arrow;
    }

    public int getCatId() {
        return catId;
    }

    public void setCatId(int catId) {
        this.catId = catId;
    }

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.key);
        dest.writeString(this.value);
        dest.writeString(this.image);
        dest.writeInt(this.arrow);
        dest.writeInt(this.catId);
        dest.writeString(this.catName);
    }

    protected ListItemModel(Parcel in) {
        this.id = in.readInt();
        this.key = in.readString();
        this.value = in.readString();
        this.image = in.readString();
        this.arrow = in.readInt();
        this.catId = in.readInt();
        this.catName = in.readString();
    }

    public static final Creator<ListItemModel> CREATOR = new Creator<ListItemModel>() {
        @Override
        public ListItemModel createFromParcel(Parcel source) {
            return new ListItemModel(source);
        }

        @Override
        public ListItemModel[] newArray(int size) {
            return new ListItemModel[size];
        }
    };
}
