package de.rs24.rugasport.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class TranslationModel implements Parcelable {

    @SerializedName("en")
    private String en = "";

    @SerializedName("de")
    private String de = "";

    @SerializedName("fr")
    private String fr = "";

    public String getEn() {
        return en;
    }

    public void setEn(String en) {
        this.en = en;
    }

    public String getDe() {
        return de;
    }

    public void setDe(String de) {
        this.de = de;
    }

    public String getFr() {
        return fr;
    }

    public void setFr(String fr) {
        this.fr = fr;
    }

    public TranslationModel() {
    }

    public TranslationModel(String en, String de, String fr) {
        this.en = en;
        this.de = de;
        this.fr = fr;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.en);
        dest.writeString(this.de);
        dest.writeString(this.fr);
    }

    protected TranslationModel(Parcel in) {
        this.en = in.readString();
        this.de = in.readString();
        this.fr = in.readString();
    }

    public static final Creator<TranslationModel> CREATOR = new Creator<TranslationModel>() {
        @Override
        public TranslationModel createFromParcel(Parcel source) {
            return new TranslationModel(source);
        }

        @Override
        public TranslationModel[] newArray(int size) {
            return new TranslationModel[size];
        }
    };
}
