package de.rs24.rugasport.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import okhttp3.Response;

public class SuccessModel implements Parcelable {
    @SerializedName("message")
    private String message;
    @SerializedName("response")
    private Response response;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }

    public SuccessModel() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.message);
    }

    protected SuccessModel(Parcel in) {
        this.message = in.readString();
        this.response = in.readParcelable(Response.class.getClassLoader());
    }

    public static final Creator<SuccessModel> CREATOR = new Creator<SuccessModel>() {
        @Override
        public SuccessModel createFromParcel(Parcel source) {
            return new SuccessModel(source);
        }

        @Override
        public SuccessModel[] newArray(int size) {
            return new SuccessModel[size];
        }
    };
}
