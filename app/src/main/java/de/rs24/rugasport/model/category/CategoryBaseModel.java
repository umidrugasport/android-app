package de.rs24.rugasport.model.category;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class CategoryBaseModel implements Parcelable {

    @SerializedName("id")
    private int id;
    @SerializedName("parent_id")
    private int parent_id;
    @SerializedName("name")
    private String name;
    @SerializedName("is_active")
    private Boolean is_active;
    @SerializedName("position")
    private int position;
    @SerializedName("level")
    private int level;
    @SerializedName("product_count")
    private int product_count;

    public CategoryBaseModel() {
    }

    public CategoryBaseModel(int id, int parent_id, String name, Boolean is_active, int position, int level, int product_count) {
        this.id = id;
        this.parent_id = parent_id;
        this.name = name;
        this.is_active = is_active;
        this.position = position;
        this.level = level;
        this.product_count = product_count;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getParent_id() {
        return parent_id;
    }

    public void setParent_id(int parent_id) {
        this.parent_id = parent_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getIs_active() {
        return is_active;
    }

    public void setIs_active(Boolean is_active) {
        this.is_active = is_active;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getProduct_count() {
        return product_count;
    }

    public void setProduct_count(int product_count) {
        this.product_count = product_count;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeInt(this.parent_id);
        dest.writeString(this.name);
        dest.writeValue(this.is_active);
        dest.writeInt(this.position);
        dest.writeInt(this.level);
        dest.writeInt(this.product_count);
    }

    protected CategoryBaseModel(Parcel in) {
        this.id = in.readInt();
        this.parent_id = in.readInt();
        this.name = in.readString();
        this.is_active = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.position = in.readInt();
        this.level = in.readInt();
        this.product_count = in.readInt();
    }

    public static final Creator<CategoryBaseModel> CREATOR = new Creator<CategoryBaseModel>() {
        @Override
        public CategoryBaseModel createFromParcel(Parcel source) {
            return new CategoryBaseModel(source);
        }

        @Override
        public CategoryBaseModel[] newArray(int size) {
            return new CategoryBaseModel[size];
        }
    };
}
