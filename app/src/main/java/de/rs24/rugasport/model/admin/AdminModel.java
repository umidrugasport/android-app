package de.rs24.rugasport.model.admin;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class AdminModel implements Parcelable {

    @SerializedName("username")
    private String username;
    @SerializedName("password")
    private String password;

    public AdminModel(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public AdminModel() {

    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.username);
        dest.writeString(this.password);
    }

    protected AdminModel(Parcel in) {
        this.username = in.readString();
        this.password = in.readString();
    }

    public static final Parcelable.Creator<AdminModel> CREATOR = new Parcelable.Creator<AdminModel>() {
        @Override
        public AdminModel createFromParcel(Parcel source) {
            return new AdminModel(source);
        }

        @Override
        public AdminModel[] newArray(int size) {
            return new AdminModel[size];
        }
    };
}
