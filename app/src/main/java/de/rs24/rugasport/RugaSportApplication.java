package de.rs24.rugasport;

import android.support.multidex.MultiDexApplication;

public class RugaSportApplication extends MultiDexApplication {

    private RugaSportComponent rugaSportComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        rugaSportComponent = DaggerRugaSportComponent.builder()
                .rugaSportModule(new RugaSportModule(this))
                .build();
    }

    public RugaSportComponent getRugaSportComponent() {
        return rugaSportComponent;
    }
}