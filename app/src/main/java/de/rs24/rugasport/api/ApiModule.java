package de.rs24.rugasport.api;

import android.content.SharedPreferences;

import com.google.gson.Gson;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import de.rs24.rugasport.api.admin.AdminApi;
import de.rs24.rugasport.api.category.CategoryApi;
import de.rs24.rugasport.api.product.ProductApi;
import de.rs24.rugasport.api.search.SearchApi;

@Module
public class ApiModule {
    @Provides
    ApiFactory provideApiFactory(SharedPreferences sharedPreferences, Gson gson) {
        return new ApiFactory(sharedPreferences,gson);
    }

    @Provides
    @Singleton
    AdminApi provideAdminApi(ApiFactory apiFactory) {
        return apiFactory.createAdminApi();
    }

    @Provides
    @Singleton
    CategoryApi provideCategoryApi(ApiFactory apiFactory) {
        return apiFactory.createCategoryApi();
    }

    @Provides
    @Singleton
    ProductApi provideProductApi(ApiFactory apiFactory) {
        return apiFactory.createProductApi();
    }

    @Provides
    @Singleton
    SearchApi provideSearchApi(ApiFactory apiFactory) {
        return apiFactory.createSearchApi();
    }
}
