package de.rs24.rugasport.api.admin;

import de.rs24.rugasport.api.ApiConstants;
import de.rs24.rugasport.model.admin.AdminModel;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface AdminApi {
    @POST(ApiConstants.ADMIN_TOKEN)
    Call<String> getAdminToken(@Body AdminModel imageModel);
}
