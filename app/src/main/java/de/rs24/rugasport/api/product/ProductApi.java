package de.rs24.rugasport.api.product;

import de.rs24.rugasport.api.ApiConstants;
import de.rs24.rugasport.model.product.ItemsModel;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

public interface ProductApi {
    @GET(ApiConstants.PRODUCTS)
    Call<ItemsModel> getTopProducts(@Header("Authorization") String authorization,
                                    @Query(ApiConstants.FIELDS) String fields,
                                    @Query("searchCriteria[filter_groups][0][filters][0][field]") String field,
                                    @Query("searchCriteria[filter_groups][0][filters][0][value]") int value,
                                    @Query("searchCriteria[filter_groups][0][filters][0][condition_type]") String conditionType,
                                    @Query("searchCriteria[pageSize]") int pageSize,
                                    @Query("searchCriteria[currentPage]") int currentPage);
}
