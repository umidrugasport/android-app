package de.rs24.rugasport.api;

import android.content.Context;

import de.rs24.rugasport.R;
import de.rs24.rugasport.cache.RugaSportCacheManager;
import de.rs24.rugasport.model.ErrorModel;
import de.rs24.rugasport.model.TranslationModel;

public class ErrorInterceptor {
    public static final String INVALID_TOKEN = "invalid token";
    public static final TranslationModel UNKNOWN_TRANSLATION = new TranslationModel();

    Context context;
    ErrorModel errorModel;
    RugaSportCacheManager cacheManager;

    public ErrorInterceptor(Context context, ErrorModel errorModel, RugaSportCacheManager cacheManager) {
        this.context = context;
        this.errorModel = errorModel;
        this.cacheManager = cacheManager;
    }

    public int getErrorCode() {
        return errorModel.getCode();
    }
    public String getMessage() {
        return handleMessage(errorModel.getMessage());
    }
    private String handleMessage(String message) {
        if(message.equals(INVALID_TOKEN)||message.equals(context.getResources().getString(R.string.invalid_token))) {
            //cacheManager.clearCache();
        }
        return message;
    }
}

