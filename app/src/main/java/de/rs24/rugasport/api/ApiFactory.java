package de.rs24.rugasport.api;

import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.Gson;

import java.io.IOException;

import de.rs24.rugasport.BuildConfig;
import de.rs24.rugasport.RugaSportDefaults;
import de.rs24.rugasport.api.admin.AdminApi;
import de.rs24.rugasport.api.category.CategoryApi;
import de.rs24.rugasport.api.product.ProductApi;
import de.rs24.rugasport.api.search.SearchApi;
import de.rs24.rugasport.cache.RugaSportCacheManager;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiFactory {

    private String adminToken;

    private Retrofit retrofit;

    public ApiFactory(SharedPreferences sharedPreferences, Gson gson) {
        RugaSportCacheManager cacheManager = new RugaSportCacheManager();
        this.adminToken = sharedPreferences.getString(cacheManager.ADMIN_TOKEN_KEY,cacheManager.EMPTY_STRING);
        retrofit = new Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_URL+ApiConstants.API_PATH)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(getClient())
                .build();
    }

    private OkHttpClient getClient() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient().newBuilder().addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Interceptor.Chain chain) throws IOException {
                Request originalRequest = chain.request();
                Request.Builder builder;
                RugaSportCacheManager cacheManager = new RugaSportCacheManager();
                Log.i(RugaSportDefaults.LOG_TAG,adminToken);
                builder = originalRequest.newBuilder();//.header("Authorization", "Bearer "+adminToken);
                Request newRequest = builder.build();
                return chain.proceed(newRequest);
            }
        }).addInterceptor(logging).build();
        return client;
    }

    public AdminApi createAdminApi() {
        return retrofit.create(AdminApi.class);
    }
    public CategoryApi createCategoryApi() {
        return retrofit.create(CategoryApi.class);
    }
    public ProductApi createProductApi() {
        return retrofit.create(ProductApi.class);
    }
    public SearchApi createSearchApi() {
        return retrofit.create(SearchApi.class);
    }
}

