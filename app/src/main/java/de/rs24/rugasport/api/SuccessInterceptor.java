package de.rs24.rugasport.api;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import de.rs24.rugasport.model.BaseModel;

public class SuccessInterceptor<T> implements Parcelable {

    private T model;
    Context context;

    @SerializedName("message")
    private String message;

    public SuccessInterceptor() {

    }

    public SuccessInterceptor(Context context, T model) {
        this.context = context;
        this.model = model;
    }

    public T getModel() {
        return model;
    }

    public void setModel(T model) {
        this.model = model;
    }

    public String getMessage() {
        return "message";
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.message);
    }

    protected SuccessInterceptor(Parcel in) {
        this.context = in.readParcelable(Context.class.getClassLoader());
        this.message = in.readString();
    }

    public static final Parcelable.Creator<SuccessInterceptor> CREATOR = new Parcelable.Creator<SuccessInterceptor>() {
        @Override
        public SuccessInterceptor createFromParcel(Parcel source) {
            return new SuccessInterceptor(source);
        }

        @Override
        public SuccessInterceptor[] newArray(int size) {
            return new SuccessInterceptor[size];
        }
    };
}
