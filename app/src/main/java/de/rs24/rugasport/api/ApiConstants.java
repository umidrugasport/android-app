package de.rs24.rugasport.api;

public class ApiConstants {
    public static final String API_PATH = "rest/V1/";
    public static final String ADMIN_TOKEN = "integration/admin/token";
    public static final String CATEGORY = "categories";
    public static final String PRODUCTS = "products";
    public static final String SEARCH = "search";
    public static final String FIELDS = "fields";
    public static final String QUICK_SEARCH_CONTAINER = "quick_search_container";
    public static final String SEARCH_TERM = "search_term";
    public static final String CATEGORY_ID = "category_id";
    public static final String ENTITY_ID = "entity_id";
}
