package de.rs24.rugasport.api.search;

import de.rs24.rugasport.api.ApiConstants;
import de.rs24.rugasport.model.product.ItemsModel;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

public interface SearchApi {
    @GET(ApiConstants.SEARCH)
    Call<ItemsModel> quickSearch(@Header("Authorization") String authorization,
                                    @Query("searchCriteria[requestName]") String requestName,
                                    @Query("searchCriteria[filter_groups][0][filters][0][field]") String field,
                                    @Query("searchCriteria[filter_groups][0][filters][0][value]") String value);

    @GET(ApiConstants.PRODUCTS)
    Call<ItemsModel> searchProductById(@Header("Authorization") String authorization,
                                        @Query("searchCriteria[filter_groups][0][filters][0][value]") int id,
                                        @Query("searchCriteria[filter_groups][0][filters][0][field]") String field);
}


