package de.rs24.rugasport.api;

import android.content.Context;
import android.util.Log;

import org.json.JSONObject;

import java.net.UnknownHostException;

import de.rs24.rugasport.R;
import de.rs24.rugasport.RugaSportDefaults;
import de.rs24.rugasport.cache.RugaSportCacheManager;
import de.rs24.rugasport.exceptions.ApiException;
import de.rs24.rugasport.model.BaseModel;
import de.rs24.rugasport.model.ErrorModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RugaSportCallbackConverter<T> implements Callback<T> {
    private RugaSportCallback<T> rugaCallback;
    private Context context;
    RugaSportCacheManager cacheManager;

    public RugaSportCallbackConverter(RugaSportCallback<T> rugaCallback, Context context, RugaSportCacheManager cacheManager) {
        this.rugaCallback = rugaCallback;
        this.context = context;
        this.cacheManager = cacheManager;
    }

    @Override
    public void onResponse(Call<T> call, Response<T> response) {
        if (response.body() != null) {
            SuccessInterceptor successInterceptor = new SuccessInterceptor();
            T model = (T) response.body();
            if(model instanceof BaseModel) {
                successInterceptor.setModel(model);
                rugaCallback.onSuccess((T) successInterceptor.getModel());
            }else{
                rugaCallback.onSuccess((T) response.body());
            }

        } else if (response.errorBody() != null) {
            try {
                if (response.errorBody().toString().contains("okhttp")) {
                    JSONObject jObjError = new JSONObject(response.errorBody().string());
                    ErrorModel errorModel = new ErrorModel();
                    errorModel.setCode(response.code());
                    if(jObjError.has("message")) {
                        errorModel.setMessage(jObjError.getString("message"));
                    }
                    rugaCallback.onFailure(new ApiException(new ErrorInterceptor(context, errorModel, cacheManager)));
                } else {
                    rugaCallback.onFailure(new ApiException(response.errorBody().toString()));
                }
            } catch (Exception ex) {
                rugaCallback.onFailure(new ApiException(response.errorBody().toString()));
            }
        } else {
            rugaCallback.onSuccess(response.body());
        }
    }

    @Override
    public void onFailure(Call<T> call, Throwable t) {
        Log.i(RugaSportDefaults.LOG_TAG,"API-Throwable-ERROR: "+t.getMessage());
        if (t instanceof UnknownHostException) {
            rugaCallback.onFailure(new ApiException(context.getString(R.string.app_no_connection)));
        } else {
            rugaCallback.onFailure(new ApiException(t.getMessage()));
        }
    }
}
