package de.rs24.rugasport.api;


import de.rs24.rugasport.exceptions.ApiException;

public interface RugaSportCallback<T> {
    void onSuccess(T response);
    void onFailure(ApiException ex);
}
