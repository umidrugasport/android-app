package de.rs24.rugasport.api.category;

import de.rs24.rugasport.api.ApiConstants;
import de.rs24.rugasport.model.category.CategoryModel;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;

public interface CategoryApi {
    @GET(ApiConstants.CATEGORY)
    Call<CategoryModel> get(@Header("Authorization") String authorization);
}
