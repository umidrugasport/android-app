package de.rs24.rugasport.exceptions;

import java.util.Locale;

import de.rs24.rugasport.RugaSportDefaults;
import de.rs24.rugasport.api.ErrorInterceptor;

public class ApiException extends Exception {

    public  String message;

    ErrorInterceptor errorInterceptor = null;

    public ApiException(String message) {
        super(message);
    }

    public ApiException(Throwable t) {
        super(t);
    }

    public ApiException(ErrorInterceptor errorInterceptor) {
        this.errorInterceptor = errorInterceptor;
    }

    @Override
    public String getMessage() {
        String message;
        if (errorInterceptor != null) {
            message = errorInterceptor.getMessage();
        } else {
            message = super.getMessage();
        }
        return message;
    }
}

