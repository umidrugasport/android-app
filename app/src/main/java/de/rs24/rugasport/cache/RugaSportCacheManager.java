package de.rs24.rugasport.cache;

import android.content.SharedPreferences;

import com.google.gson.Gson;

import javax.inject.Inject;

import de.rs24.rugasport.model.category.CategoryModel;

public class RugaSportCacheManager {

    public final String EMPTY_JSON = "{}";
    public final String ADMIN_TOKEN_KEY = "admintokenkey";
    public final String EMPTY_STRING = "";
    private final String TOKEN_KEY = "token";
    private final String USER_ID_KEY = "userid";
    private final String USER_KEY = "user";
    private final String CATEGORIES_KEY = "categories";
    private final String LAST_ACTIVITY_KEY = "lastActivity";
    private final String LAST_FRAGMENT_KEY = "lastFragment";
    private final String LAST_CAT_ID_KEY = "lastCatId";
    private final String LAST_CAT_NAME_KEY = "lastCatName";
    private final String LAST_PAGE_KEY = "lastPage";

    SharedPreferences sharedPreferences;
    Gson gson;

    @Inject
    public RugaSportCacheManager(SharedPreferences sharedPreferences, Gson gson) {
        this.sharedPreferences = sharedPreferences;
        this.gson = gson;
    }

    public RugaSportCacheManager() {

    }

    public RugaSportCacheManager(SharedPreferences sharedPreferences) {
        this.sharedPreferences = sharedPreferences;
    }

    public void saveAdminToken(String token) {
        sharedPreferences.edit().putString(ADMIN_TOKEN_KEY, token).apply();
    }
    public String getAdminToken() {
        return sharedPreferences.getString(ADMIN_TOKEN_KEY, EMPTY_STRING);
    }
    public void deleteAdminToken() {
        sharedPreferences.edit().putString(ADMIN_TOKEN_KEY, EMPTY_STRING).apply();
    }

    /*
    * CATEGORIES
    */
    public void saveCategories(CategoryModel categories) {
        sharedPreferences.edit().putString(CATEGORIES_KEY, gson.toJson(categories)).apply();
    }
    public CategoryModel getCategories() {
        String categoryModelJson = sharedPreferences.getString(CATEGORIES_KEY, EMPTY_JSON);
        return gson.fromJson(categoryModelJson, CategoryModel.class);
    }
    public void clearCategories() {
        saveCategories(new CategoryModel());
    }

    /*
    * FRAGMENT HISTORY
    */
    public String getLastActivity() {
        return sharedPreferences.getString(LAST_ACTIVITY_KEY, EMPTY_STRING);
    }
    public void saveLastActivity(String activity) {
        sharedPreferences.edit().putString(LAST_ACTIVITY_KEY, activity).apply();
    }
    public String getLastFragment() {
        return sharedPreferences.getString(LAST_FRAGMENT_KEY, EMPTY_STRING);
    }
    public void saveLastFragment(String fragment) {
        sharedPreferences.edit().putString(LAST_FRAGMENT_KEY, fragment).apply();
    }
    public String getLastCatId() {
        return sharedPreferences.getString(LAST_CAT_ID_KEY, EMPTY_STRING);
    }
    public void saveLastCatId(String catId) {
        sharedPreferences.edit().putString(LAST_CAT_ID_KEY, catId).apply();
    }
    public String getLastCatName() {
        return sharedPreferences.getString(LAST_CAT_NAME_KEY, EMPTY_STRING);
    }
    public void saveLastCatName(String catName) {
        sharedPreferences.edit().putString(LAST_CAT_NAME_KEY, catName).apply();
    }
    public String getLastPage() {
        return sharedPreferences.getString(LAST_PAGE_KEY, "1");
    }
    public void saveLastPage(String page) {
        sharedPreferences.edit().putString(LAST_PAGE_KEY, page).apply();
    }
}
