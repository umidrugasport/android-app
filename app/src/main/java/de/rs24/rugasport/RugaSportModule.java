package de.rs24.rugasport;

import android.app.NotificationManager;
import android.content.Context;
import android.net.ConnectivityManager;

import com.squareup.otto.Bus;
import com.squareup.otto.ThreadEnforcer;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import de.rs24.rugasport.api.ApiModule;

@Module(
        includes = {
                AndroidModule.class,
                ApiModule.class
        }
)
public class RugaSportModule {
        Context context;

        public RugaSportModule(Context context) {
                this.context = context;
        }

        public RugaSportModule() {

        }

        @Provides
        @Singleton
        Context provideContext() {
                return context;
        }

        @Provides
        @Singleton
        NotificationManager provideNotificationManager(Context context) {
                return (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        }

        @Provides
        @Singleton
        ConnectivityManager provideConectivityMAnager(Context context) {
                return (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        }

        @Provides
        @Singleton
        Bus provideBus() {
                return new Bus(ThreadEnforcer.ANY);
        }
}